/**
 * Converts Hex to RGB string
 * @param {string} color 0-ff
 * @returns {string} RGB value
 */
module.exports = {
    hexToRGB: (color) => {
        let r = 0, g = 0, b = 0;
      
        // 3 digits
        if (color.length == 3) {
            r = "0x" + color[0] + color[0];
            g = "0x" + color[1] + color[1];
            b = "0x" + color[2] + color[2];
      
        // 6 digits
        } else if (color.length == 6) {
            r = "0x" + color[0] + color[1];
            g = "0x" + color[2] + color[3];
            b = "0x" + color[4] + color[5];
        }
        
        return "rgb("+ +r + ", " + +g + ", " + +b + ")";
    }
}