const express = require("express");
const converter = require("./converter");

const app = express();
const port = 3000;

app.get('/', (req, res) => {
    res.send("Hello");
});

app.get('/hex-to-rgb', (req, res) => {
    const color = req.query.color;
    const rgb = converter.hexToRGB(color);

    res.send(rgb);
});

if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} 

else {
    app.listen(port, () => {
        console.log(`Server: localhost:${port}`);
    });
}