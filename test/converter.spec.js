// TDD - Unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

// Test color converting
describe("Color Code Converter", () => {
    describe("Hex to RGB conversion", () => {
        it("Converts the basic colors", () => {
            const redRGB = converter.hexToRGB("ff0000");   // Red
            const greenRGB = converter.hexToRGB("00ff00"); // Green
            const blueRGB = converter.hexToRGB("0000ff");  // Blue

            expect(redRGB).to.equal("rgb(255, 0, 0)");
            expect(greenRGB).to.equal("rgb(0, 255, 0)");
            expect(blueRGB).to.equal("rgb(0, 0, 255)");
        });
    });
});