// Integration test

const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

describe("Color Code Converter API", () => {
    // Start server
    before("Start server before run tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening: localhost:${port}`);
            done();
        });
    });

    
    describe("Hex to RGB conversion", () => {
        // Test connection
        const url = `http://localhost:${port}/hex-to-rgb?color=ffffff`;
        it("Returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        // Test color convertion
        it("Returns the color in RGB", (done) => {       
            request(url, (error, response, body) => {
                expect(body).to.equal("rgb(255, 255, 255)");
                done();
            });           
        });
    });

    // Stop server
    after("Stop server after tests", (done) => {
        server.close();
        done();
    });
})